import Vue from 'vue'
import Router from 'vue-router'
// in development env not use Lazy Loading,because Lazy Loading too many pages will cause webpack hot update too slow.so only in production use Lazy Loading

/* layout */
import Layout from '../views/layout/Layout'

const _import = require('./_import_' + process.env.NODE_ENV)

Vue.use(Router)

/**
 * icon : the icon show in the sidebarconsole
 * hidden : if `hidden:true` will not show in the sidebar
 * redirect : if `redirect:noRedirect` will not redirect in the levelBar
 * noDropDown : if `noDropDown:true` will not has submenu in the sidebar
 * meta : `{ permissao: ['a:xx'] }`  will control the page permissao
 **/
export const constantRouterMap = [
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/404', component: _import('errorPage/404'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    icon: 'dashboard',
    noDropDown: true,
    children: [{
      path: 'dashboard',
      name: 'Home',
      component: _import('dashboard/index'),
      meta: { title: 'dashboard', noCache: true }
    }]
  }
]

export default new Router({
  // mode: 'history', //O suporte de back-end está disponível
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  {
    path: '/usuario',
    component: Layout,
    redirect: '/usuario/list',
    icon: 'name',
    noDropDown: true,
    children: [{
      path: 'list',
      name: 'Usuáro',
      component: _import('usuario/list'),
      meta: { permissao: ['usuario:list'] }
    }]
  },

  {
    path: '/usuario',
    component: Layout,
    redirect: '/usuario/detail',
    hidden: true,
    children: [{
      path: 'detail',
      name: 'Perfil',
      component: _import('usuario/detail')
    }]
  },

  {
    path: '/grupo',
    component: Layout,
    redirect: '/grupo/list',
    icon: 'role',
    noDropDown: true,
    children: [{
      path: 'list',
      name: 'Grupo',
      component: _import('grupo/list'),
      meta: { permissao: ['grupo:list'] }
    }]
  }
]
