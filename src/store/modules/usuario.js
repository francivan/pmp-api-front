import { login, logout, detail } from '@/api/usuario'
import { getToken, setToken, removeToken } from '@/utils/token'

const usuario = {
  state: {
    token: getToken(),
    idUsuario: -1,
    email: null,
    username: null,
    ultimoAcesso: -1,
    dataCadastro: -1,
    grupoNome: null,
    permissaoActionList: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_ACCOUNT: (state, usuario) => {
      state.idUsuario = usuario.idUsuario
      state.email = usuario.email
      state.username = usuario.username
      state.ultimoAcesso = usuario.ultimoAcesso
      state.dataCadastro = usuario.dataCadastro
      state.grupoNome = usuario.grupoNome
      state.permissaoActionList = usuario.permissaoActionList
    },
    RESET_ACCOUNT: (state) => {
      state.token = null
      state.idUsuario = -1
      state.email = null
      state.username = null
      state.ultimoAcesso = -1
      state.dataCadastro = -1
      state.grupoNome = null
      state.permissaoActionList = []
    }
  },

  actions: {
    // 登录
    Login({ commit }, loginForm) {
      return new Promise((resolve, reject) => {
        login(loginForm).then(response => {
          if (response.code === 200) {
            // cookie中保存token
            setToken(response.data)
            // vuex中保存token
            commit('SET_TOKEN', response.data)
          }
          // 传递给/login/index.vue : store.dispatch('Login').then(data)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    Detail({ commit }) {
      return new Promise((resolve, reject) => {
        detail().then(response => {
          // 储存用户信息
          commit('SET_ACCOUNT', response.data)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout({ commit }) {
      return new Promise((resolve, reject) => {
        logout().then(() => {
          // 清除token等相关角色信息
          commit('RESET_ACCOUNT')
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogout({ commit }) {
      return new Promise(resolve => {
        commit('RESET_ACCOUNT')
        removeToken()
        resolve()
      })
    }
  }
}

export default usuario
