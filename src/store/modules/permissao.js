import { asyncRouterMap, constantRouterMap } from '@/router/index'

/**
 * Determine se ele corresponde às permissões do usuário atual por meio do meta.auth
 * @param permissaoActionList
 * @param route
 */
function possuiPermissao(permissaoActionList, route) {
  if (route.meta && route.meta.permissao) {
    return permissaoActionList.some(permissao => route.meta.permissao.indexOf(permissao) >= 0)
  } else {
    return true
  }
}

/**
 * Filtre recursivamente a tabela de roteamento assíncrona e retorne a tabela de roteamento que corresponde às permissões de função do usuário
 * @param asyncRouterMap
 * @param permissaoActionList
 */
function filterAsyncRouter(asyncRouterMap, permissaoActionList) {
  return asyncRouterMap.filter(route => {
    // filter, o método de filtragem e filtragem de arrays na sintaxe js
    if (possuiPermissao(permissaoActionList, route)) {
      if (route.children && route.children.length) {
        // Se houver um próximo nível abaixo desta rota, ele é chamado recursivamente
        route.children = filterAsyncRouter(route.children, permissaoActionList)
        // Se após filtrar um círculo não houver elementos filhos, o menu pai não será exibido.
        // return (route.children && route.children.length)
      }
      return true
    }
    return false
  })
}

const permissao = {
  state: {
    routers: constantRouterMap, // Todas as rotas deste usuário, incluindo rotas fixas e os seguintes addRouters
    addRouters: [] // Novas rotas dinâmicas atribuídas pela função deste usuário
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers) // Combine a rota fixa e a nova rota para se tornar a informação final de roteamento do usuário
    }
  },
  actions: {
    GenerateRoutes({ commit }, usuario) {
      return new Promise(resolve => {
        const grupo = usuario.grupoNome
        const permissaoActionList = usuario.permissaoActionList
        // Declare as rotas disponíveis para esta função
        let accessedRouters
        if (grupo === 'superadministrador') {
          // Se a função contiver 'superadministrador', todas as rotas poderão ser usadas
          // Na verdade, o administrador também tem todos os menus. Aqui, usamos principalmente o julgamento de funções para economizar tempo de carregamento.
          accessedRouters = asyncRouterMap
        } else {
          // 否则需要通过以下方法来筛选出本角色可用的路由
          accessedRouters = filterAsyncRouter(asyncRouterMap, permissaoActionList)
        }
        // Execute o método de configuração da rota
        commit('SET_ROUTERS', accessedRouters)
        resolve()
      })
    }
  }
}

export default permissao
