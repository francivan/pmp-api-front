const getters = {
  sidebar: state => state.app.sidebar,

  token: state => state.usuario.token,
  idUsuario: state => state.usuario.idUsuario,
  email: state => state.usuario.email,
  username: state => state.usuario.username,
  ultimoAcesso: state => state.usuario.ultimoAcesso,
  dataCadastro: state => state.usuario.dataCadastro,
  grupoNome: state => state.usuario.grupoNome,
  permissaoActionList: state => state.usuario.permissaoActionList,

  permissaoRouters: state => state.permissao.routers,
  addRouters: state => state.permissao.addRouters
}
export default getters
