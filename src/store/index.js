import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import usuario from './modules/usuario'
import permissao from './modules/permissao'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    usuario,
    permissao
  },
  getters
})

export default store
