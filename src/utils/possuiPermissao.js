import store from '../store'

export function possuiPermissao(permissao) {
  return store.getters.permissaoActionList.indexOf(permissao) >= 0
}
