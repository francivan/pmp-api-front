import router from './router'
import store from './store'
import NProgress from 'nprogress' // Barra de progresso
import 'nprogress/nprogress.css'// Estilo da barra de progresso
import { getToken } from '@/utils/token'

const whiteList = ['/login'] // Lista de permissões, rotas que não exigem login

router.beforeEach((to, from, next) => {
  NProgress.start() // Iniciar progresso
  // tente pegar o token no cookie
  if (getToken()) {
    // ter token
    if (to.path === '/login') {
      // Mas o próximo salto é a página de destino
      // vá para a página inicial
      next({ path: '/' })
    } else {
      // O próximo salto não é uma página de destino
      // VUEX está limpo, sem nome de caractere
      if (store.getters.grupoNome === null) {
        // Recuperar informações do usuário
        store.dispatch('Detail').then(response => {
          // Gerar rotas
          store.dispatch('GenerateRoutes', response.data).then(() => {
            router.addRoutes(store.getters.addRouters)
            next({ ...to })
          })
        })
      } else {
        next()
      }
    }
  } else {
    // Se a rota para a qual você for estiver na lista de permissões, você poderá ir diretamente para
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      // Se o caminho não estiver na lista de permissões e você não estiver logado, vá para a página de login
      next('/login')
      NProgress.done() // Finalizar Progresso
    }
  }
})

router.afterEach(() => {
  NProgress.done() // Finalizar Progresso
})
