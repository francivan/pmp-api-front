import Vue from 'vue'
import ElementUI from 'element-ui'
import './styles/element-variables.scss'
import App from './App'
import router from './router'
import store from './store'
import '@/icons' // icon
import '@/permissao' // 权限
import { default as request } from './utils/request'
import { possuiPermissao } from './utils/possuiPermissao'
import lang from 'element-ui/lib/locale/lang/pt-br'
import locale from 'element-ui/lib/locale'

// configuração de idioma
locale.use(lang)

Vue.use(ElementUI, {
  size: 'small'
})

// constante global
Vue.prototype.request = request
Vue.prototype.possuiPermissao = possuiPermissao

// Definido automaticamente como falso na produção para evitar que a Web gere prompts de produção na inicialização
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
