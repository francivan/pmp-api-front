import request from '@/utils/request'

export function listGrupoComPermissao(params) {
  return request({
    url: '/grupo/permissao',
    method: 'get',
    params
  })
}

export function list(params) {
  return request({
    url: '/grupo',
    method: 'get',
    params
  })
}

export function listControllerPermissao(params) {
  return request({
    url: '/permissao',
    method: 'get',
    params
  })
}

export function add(grupoForm) {
  return request({
    url: '/grupo',
    method: 'post',
    data: grupoForm
  })
}

export function update(grupoForm) {
  return request({
    url: '/grupo',
    method: 'put',
    data: grupoForm
  })
}

export function remove(idGrupo) {
  return request({
    url: '/grupo/' + idGrupo,
    method: 'delete'
  })
}

export function updateUsuarioGrupo(usuario) {
  return request({
    url: '/usuario/grupo',
    method: 'put',
    data: usuario
  })
}
