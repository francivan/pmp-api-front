import request from '@/utils/request'

export function search(searchForm) {
  return request({
    url: '/usuario/search',
    method: 'post',
    data: searchForm
  })
}

export function list(params) {
  return request({
    url: '/usuario',
    method: 'get',
    params
  })
}

export function validatePassword(usuarioForm) {
  return request({
    url: '/usuario/password',
    method: 'post',
    data: usuarioForm
  })
}

export function update(usuarioForm) {
  return request({
    url: '/usuario/detail',
    method: 'put',
    data: usuarioForm
  })
}

export function updateUsuario(usuarioForm) {
  return request({
    url: '/usuario/' + usuarioForm.Id,
    method: 'put',
    data: usuarioForm
  })
}

export function remove(idUsuario) {
  return request({
    url: '/usuario/' + idUsuario,
    method: 'delete'
  })
}

export function register(usuarioForm) {
  return request({
    url: '/usuario',
    method: 'post',
    data: usuarioForm
  })
}

export function login(usuarioForm) {
  return request({
    url: '/usuario/token',
    method: 'post',
    data: usuarioForm
  })
}

export function detail() {
  return request({
    url: '/usuario/detail',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/usuario/token',
    method: 'delete'
  })
}
